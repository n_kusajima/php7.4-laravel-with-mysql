# Laravel6.x project with php7.4 on docker (alpine)

## Summary
This project is Laravel project on docker.  
And uses PHP7.4, mysql5.7 and nginx.

## Requirements
* docker
* docker-compose

## How to run
```bash
cd path/to/project
docker-compose up -d
docker-compose exec app ash

cd /var/www/html
composer install
```
and see `localhost:8888`
